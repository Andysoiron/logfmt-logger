# frozen_string_literal: true
require 'logger'

module Logfmt
  class Logger < Logger
    def initialize
      super(STDOUT)
      @formatter = Logfmt::Formatter.new
    end
  end
end

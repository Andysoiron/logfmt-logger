# frozen_string_literal: true

class Hash
  def to_logfmt
    [].tap { |messages|
      each do |key, value|
        if [true, false].include? value
          messages << key.to_s if value
        else
          value = value.to_s
          value = "\"#{value}\"" if value.include?('=') || value.include?(' ')
          messages << "#{key}=#{value}" unless value.empty?
        end
      end
    }.join(" ")
  end
end
